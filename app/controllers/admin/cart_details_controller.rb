class Admin::CartDetailsController < Admin::BaseController
  before_action :set_cart, only: [:new, :create, :destroy]

  # GET /cart_details/new
  def new
    @cart_details = @cart.cart_details.build
    @cart_details.item = Item.first
  end

  # GET /cart_details/1/edit
  def edit
    @cart_detail = CartDetail.find(params[:id])
  end

  # POST /cart_details
  # POST /cart_details.json
  def create
    item_exists = false
    item_id = params[:cart_details][:item_id].to_i
    @cart.cart_details.each do |detail|
      if detail.item_id == item_id
        # Ya existe el item en el carro, agregar cantidad
        item_exists = true
        @cart_detail = detail
        @saved_cart_detail = detail.id
        break
      end
    end
    if item_exists
      @cart_detail.qty += params[:cart_details][:qty].to_i
      @cart_detail.price = params[:cart_details][:price].to_f
      @cart_detail.save!
    else
      cart_detail = CartDetail.new(cart_detail_params)
      if @cart.cart_details.last.nil?
        cart_detail.number = 1
      else
        cart_detail.number = @cart.cart_details.last.number + 1
      end
      @cart.cart_details << cart_detail
    end
    @cart.save!
  end

  # PATCH/PUT /cart_details/1
  # PATCH/PUT /cart_details/1.json
  def update
    # respond_to do |format|
    #   if @cart_detail.update(cart_detail_params)
    #     format.html { redirect_to @cart_detail, notice: 'Cart detail was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @cart_detail }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @cart_detail.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /cart_details/1
  # DELETE /cart_details/1.json
  def destroy
    @cart_detail = CartDetail.find(params[:id])
    @cart_detail.destroy

    respond_to do |format|
      format.js { render layout: false }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart
      @cart = Cart.find(params[:cart_id].to_i)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cart_detail_params
      params.require(:cart_detail).permit(:id, :sale_id, :number, :item_id, :qty, :price, :item_description, :_destroy)
    end
end
