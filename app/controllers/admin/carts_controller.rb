class CartsController < ApplicationController
  before_action :set_cart, only: [:show, :edit, :update, :destroy]
  PAGE_SIZE = 10

  # GET /carts
  # GET /carts.json
  def index
    # Clear no saved carts:
    unsaved_carts = Cart.where(state: "draft", user: current_user)
    unsaved_carts.each do |cart|
      cart.destroy
    end

    @page = (params[:page] || 0).to_i
    @keywords = params[:keywords]

    search = Search.new(@page, PAGE_SIZE, @keywords, current_user)
    @carts, @number_of_pages = search.carts
  end

  # GET /carts/1
  # GET /carts/1.json
  def show
  end

  # GET /carts/new
  def new
    last_cart = Cart.where(state: "confirmed", user: current_user).maximum('number')
    number =  (last_cart != nil) ? last_cart + 1 : 1
    @cart = Cart.create(date: Date::current, number: number, state: "draft", user: current_user)
    @cart.cart_details.build
    params[:cart_id] = @cart.id.to_s
  end

  # GET /carts/1/edit
  def edit
  end

  # POST /carts
  # POST /carts.json
  def create
    # @cart = Cart.new(cart_params)

    # respond_to do |format|
    #   if @cart.save
    #     format.html { redirect_to @cart, notice: 'Cart was successfully created.' }
    #     format.json { render :show, status: :created, location: @cart }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @cart.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /carts/1
  # PATCH/PUT /carts/1.json
  def update
    @cart.confirmed!
    respond_to do |format|
      if @cart.update(cart_params)
        format.html { redirect_to carts_url, notice: 'Cart was successfully updated.' }
        format.json { render :show, status: :ok, location: @cart }
      else
        format.html { render :edit }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /carts/1
  # DELETE /carts/1.json
  def destroy
    @cart.destroy
    respond_to do |format|
      format.html { redirect_to carts_url, notice: 'Cart was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart
      @cart = Cart.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cart_params
      params.require(:cart).permit(:number,:date, cart_details_attributes: [:id, :cart_id, :item_id, :number, :qty, :price, :_destroy])
    end
end
