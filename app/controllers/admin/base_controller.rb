module Admin
	class BaseController < ApplicationController
		before_action :authenticate_admin!

		private
		def authenticate_admin!
			unless current_user.admin?
				redirect_to main_home_path, alert: "No tienes autorización para entrar en esta sección."
			end
		end
	end
end