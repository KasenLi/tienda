class MainController < ApplicationController
  def home
  	@items = Item.all
  end

  def set_layout
  	"landing"
  end
end
