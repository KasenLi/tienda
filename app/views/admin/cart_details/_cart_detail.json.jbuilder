json.extract! cart_detail, :id, :sale_id, :number, :item_id, :qty, :price, :created_at, :updated_at
json.url cart_detail_url(cart_detail, format: :json)
