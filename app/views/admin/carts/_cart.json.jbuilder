json.extract! cart, :id, :date, :state, :user_id, :created_at, :updated_at
json.url cart_url(cart, format: :json)
