class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :carts
  belongs_to :city

  enum role: [ :member, :admin ]

  validates :username, :name, :last_name, :birthday, :genre, :city_id, presence: true
end
