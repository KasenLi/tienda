class Item < ApplicationRecord
  belongs_to :brand
  belongs_to :unit
  belongs_to :category

  validates :description,:code,:brand_id, :unit_id, :category_id, :price, presence: true
  validates :code, uniqueness: true
  validates :stock,:price, length: { minimum: 0}
  validates :price, numericality: true
  validates :stock, numericality: { only_integer: true}

  def item_description
  	self.description + ( (self.brand != nil) ? ' ' + self.brand.name : '' )
  end

  def brand_name
  	if self.brand
  		self.brand.name
  	else
  		''
  	end
  end
end
