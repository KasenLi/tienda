class City < ApplicationRecord
	validates :code, presence: true, uniqueness: true
	validates :name, presence: true

	has_many :cities
end
