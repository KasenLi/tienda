class Cart < ApplicationRecord
	has_many :cart_details, inverse_of: :cart, dependent: :destroy
	has_many :items, through: :cart_details
	belongs_to :user

	validates :date, presence: true

	accepts_nested_attributes_for :cart_details, reject_if: :cart_detail_rejectable?,
									allow_destroy: true

	enum state: [:draft, :confirmed]

	def total
		details = self.cart_details

		total = 0.0
		details.flat_map do |d|
			total += d.qty * d.price
		end
		total
	end

	private

		def cart_detail_rejectable?(att)
			att[:item_id].blank? || att[:qty].blank? || att[:price].blank? || att[:qty].to_f <= 0 || att[:price].to_f <= 0
		end
		
end
