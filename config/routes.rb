Rails.application.routes.draw do
  
  get 'main/home'

  root 'admin/welcome#index'
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  namespace "admin" do
    resources :items
    resources :cities
    resources :units
    resources :categories
    resources :brands
    resources :cart_details
    resources :carts
    resources :users
  end

	get '/brands_suggestion', to: 'brands_suggestion#index'
	get '/validate_suggested_brand', to: 'validate_suggested_brand#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
