class CreateCarts < ActiveRecord::Migration[5.1]
  def change
    create_table :carts do |t|
      t.date :date
      t.integer :state
      t.integer :user_id

      t.timestamps
    end
  end
end
