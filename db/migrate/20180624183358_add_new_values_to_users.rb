class AddNewValuesToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :username, :string
    add_column :users, :name, :string
    add_column :users, :last_name, :string
    add_column :users, :type, :integer, default: 0 #0 Normal, 1 Admin
    add_reference :users, :city, foreign_key: true
    add_column :users, :birthday, :date
    add_column :users, :telf, :integer
  end
end
