class AddNumberToCarts < ActiveRecord::Migration[5.1]
  def change
    add_column :carts, :number, :integer
  end
end
