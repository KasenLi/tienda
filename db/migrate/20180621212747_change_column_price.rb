class ChangeColumnPrice < ActiveRecord::Migration[5.1]
  def change
  	rename_column :items, :precio, :price
  end
end
